package com.javastart.beansscope;

import com.javastart.beansscope.service.BaseService2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class Application implements CommandLineRunner {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
    @Autowired
    private ApplicationContext applicationContext;
    public void run(String... args) throws Exception { // get bean method

        BaseService2 baseService2 = (BaseService2) applicationContext.getBean("BaseService2");

        baseService2.run();

        AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();

        beanFactory.destroyBean(baseService2);
    }
}
