package com.javastart.beansscope.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;


@Service
public class BaseService implements CommandLineRunner { // reading and changing
    @Autowired
    private AccountServicePrototype accountServicePrototype;
    @Autowired
    private AccountServiceSingleton accountServiceSingleton;
    public void run(String... args) throws Exception {
        System.out.println("========Prototype========");
        accountServicePrototype.changeName("Baks");
        System.out.println(accountServicePrototype.getName());

        System.out.println("==========Singleton========");
        accountServiceSingleton.changeName("Baks");
        System.out.println(accountServiceSingleton.getName());
    }
}
