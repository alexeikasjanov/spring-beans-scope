package com.javastart.beansscope.service;


import org.springframework.stereotype.Service;

@Service
public class AccountServiceSingleton {

    private String name = "Lori";

    public String getName() {
        return name;
    }
    public void changeName(String name){
         this.name = name + "---"+name;
    }
}
